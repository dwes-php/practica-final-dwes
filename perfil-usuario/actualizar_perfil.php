<?php
session_start();

if (isset($_SESSION["error_pass"])) {
    $error_pass = $_SESSION["error_pass"];

    unset($_SESSION["error_pass"]);
} else {

    $error_pass = "";
}

/*con alex
$ip = "192.168.56.101:3306";
$database = "DWES-php";
$user = "admin";
$pass = "admin";*/

/*conexion sami*/
$ip = "localhost";
$database = "DWES-php";
$user = "root";
$pass = "Samanta3007";

$conexion = mysqli_connect($ip, $user, $pass) or die("No se ha podido conectar a la base de datos");
mysqli_select_db($conexion, $database) or die("No existe la base de datos");

$id = $_SESSION['id'];

// Verificar si se enviaron datos por el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtener datos 
    $apellido = $_POST["apellidos"];
    $nombre = $_POST["nombre"];
    $email = $_POST["email"];
    $contraseña_antigua = $_POST["contraseña_antigua"];
    $contraseña_nueva = $_POST["contraseña_nueva"];


    // actualizar
    $actualizar = "UPDATE usuarios SET nombre = '$nombre', apellido = '$apellido', email = '$email' WHERE id = $id";
    mysqli_query($conexion, $actualizar);

    // nueva contraseña
    $consulta_contraseña = "SELECT pass FROM usuarios WHERE id = $id";
    $resultado = mysqli_query($conexion, $consulta_contraseña);
    if ($resultado && mysqli_num_rows($resultado) > 0) {
        $fila = mysqli_fetch_assoc($resultado);
        $contraseña_bd = $fila['pass'];

        // Verificar si la contraseña antigua coincide con la almacenada en la base de datos
        if (password_verify($contraseña_antigua, $contraseña_bd)) {
            // La contraseña antigua coincide, encriptar y actualizar la nueva contraseña
            $contraseña_encriptada_nueva = password_hash($contraseña_nueva, PASSWORD_DEFAULT);
            $actualizar_contraseña = "UPDATE usuarios SET pass = '$contraseña_encriptada_nueva' WHERE id = $id";
            if (mysqli_query($conexion, $actualizar_contraseña)) {
                echo "";
                header("Location: actualizar_perfil.php");
            } else {
                echo "Error al actualizar la contraseña: " . mysqli_error($conexion);
            }
        } else {
            $_SESSION["error_pass"] = "!!La contraseña antigua no coincide";
            header("Location: actualizar_perfil.php");
            exit();
        }
    } else {
        echo "Error al obtener la contraseña almacenada.";
    }
}

// Obtener datos del usuario
$consulta = "SELECT * FROM usuarios WHERE id = $id";
$resultado = mysqli_query($conexion, $consulta);
$row = mysqli_fetch_assoc($resultado);

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perfil</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bungee+Spice&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="perfil.css">
</head>

<body>
    <header class="head">

        <nav class="logo">
            <a href="../indexpeli.php">
                <h1>FLIXHUB</h1>
            </a>
        </nav>

        <nav class="perfil">
            <?php

            // Consulta para obtener la foto de perfil asociada al email
            $sql = "SELECT fotos_perfil.nombre_archivo 
              FROM usuarios 
              INNER JOIN fotos_perfil ON usuarios.foto_perfil_id = fotos_perfil.id 
              WHERE usuarios.id = '$id'";

            // Ejecutar la consulta
            $resultado = $conexion->query($sql);

            // Verificar si se encontró la foto de perfil
            if ($resultado->num_rows > 0) {
                // Obtener el primer resultado
                $fila = $resultado->fetch_assoc();
                $foto_perfil = $fila['nombre_archivo'];
                // Mostrar la foto de perfil en la página
                echo '<div class="foto_perfil"><img src="../' . $foto_perfil . '" alt="Foto de perfil"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M169.4 470.6c12.5 12.5 32.8 12.5 45.3 0l160-160c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L224 370.8 224 64c0-17.7-14.3-32-32-32s-32 14.3-32 32l0 306.7L54.6 265.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l160 160z"/></svg></div>';
            } else {
                echo "No se encontró ninguna foto de perfil asociada al email '$id'";
            }

            ?>

            <div class="salir">
                <a href="cerrar-sesion.php"><button>Cerrar sesion</button></a>
            </div>
        </nav>

    </header>
    <div class="line">
        <!-- en blanco -->
    </div>

    <section class="formulario">
        <h1 class="details">Detalles de la cuenta</h1>
        <div class="box">

            <!-- Formulario para actualizar los datos del usuario -->
            <form action="actualizar_perfil.php" method="post" enctype="multipart/form-data">
                <div class="icons">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                        <path d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                    </svg>
                    <div class="labels">
                        <label for="nombre">Nombre</label>
                        <input type="text" id="nombre" name="nombre" value="<?php echo $row['nombre']; ?>"><br><br>
                    </div>
                </div>

                <div class="icons">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                        <path d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                    </svg>
                    <div class="labels">
                        <label for="apellidos">Apellidos:</label>
                        <input type="text" id="apellidos" name="apellidos" value="<?php echo $row['apellido']; ?>"><br><br>
                    </div>
                </div>

                <div class="icons">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                        <path d="M48 64C21.5 64 0 85.5 0 112c0 15.1 7.1 29.3 19.2 38.4L236.8 313.6c11.4 8.5 27 8.5 38.4 0L492.8 150.4c12.1-9.1 19.2-23.3 19.2-38.4c0-26.5-21.5-48-48-48H48zM0 176V384c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V176L294.4 339.2c-22.8 17.1-54 17.1-76.8 0L0 176z" />
                    </svg>
                    <div class="labels">
                        <label for="email">Email:</label>
                        <input type="email" id="email" name="email" value="<?php echo $row['email']; ?>"><br><br>
                    </div>
                </div>

                <div class="icons">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                        <path d="M336 352c97.2 0 176-78.8 176-176S433.2 0 336 0S160 78.8 160 176c0 18.7 2.9 36.8 8.3 53.7L7 391c-4.5 4.5-7 10.6-7 17v80c0 13.3 10.7 24 24 24h80c13.3 0 24-10.7 24-24V448h40c13.3 0 24-10.7 24-24V384h40c6.4 0 12.5-2.5 17-7l33.3-33.3c16.9 5.4 35 8.3 53.7 8.3zM376 96a40 40 0 1 1 0 80 40 40 0 1 1 0-80z" />
                    </svg>
                    <div class="labels">
                        <label for="contraseña_antigua">Contraseña antigua</label>
                        <input type="password" id="contraseña_antigua" name="contraseña_antigua" required>
                        <label for="contraseña">Contraseña nueva:</label>
                        <input type="password" id="contraseña" name="contraseña_nueva" required><br><br>
                    </div>
                </div>

                <div class="fotos_perfil">
                    <label for="foto_perfil">Foto de perfil:</label>
                    <div class="labels_photos">

                        <?php
                        // Obtener las imágenes disponibles de la base de datos
                        $query = "SELECT id, nombre_archivo FROM fotos_perfil";
                        $result = mysqli_query($conexion, $query);
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo "<label><input type='radio' name='foto_perfil' value='{$row['id']}'></label>";
                            echo "<div class='photos'><img src='../{$row['nombre_archivo']}' alt='Foto de perfil'></div>";
                        }

                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                            // Verificar si se ha seleccionado una imagen de perfil
                            if (isset($_POST['foto_perfil']) && !empty($_POST['foto_perfil'])) {
                                // Capturar el valor del ID de la imagen de perfil seleccionada
                                $id_foto_perfil = $_POST['foto_perfil'];

                                $imagen = "UPDATE usuarios SET foto_perfil_id = '$id_foto_perfil' WHERE id = '$id'";
                                mysqli_query($conexion, $imagen);
                                // Ejecutar la consulta
                            }
                        }
                        ?>
                    </div>
                </div>

                <br>
                <div class="save">
                    <input type="submit" value="Guardar cambios">
                </div>
                <?php
                if (!empty($error_pass)) {
                    echo '<p class="msg">' . $error_pass . '</p>';
                }
                ?>
            </form>
        </div>
    </section>
    <div class="line">
        <!-- en blanco -->
    </div>

    <footer class="footer">
        <div class="contain">

            <div class="copy">
                <small>&copy; 2023 <b>-SA-</b> Alexander Imba Paucar & Samanta Luciana Dallesso.</small>
            </div>

            <div class="contact">
                <div>
                    <h1>Contact Us</h1>
                    <div class="line1"></div>
                </div>
                <div class="text">
                    <p><a href="mailto:alexander.dev340@gmail.com">alexander.dev340@gmail.com</a></p>
                    <p><a href="mailto:samanta.luciana@hotmail.com">samanta.luciana@hotmail.com</a></p>
                </div>
            </div>

        </div>
    </footer>
</body>

</html>