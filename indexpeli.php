<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Ejercicio Final</title>
  <link rel="stylesheet" href="stylespeli.css" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Bungee+Spice&display=swap" rel="stylesheet">
</head>
</head>
<?php
session_start();

// Verificar si el usuario es admin
if (isset($_SESSION['admin']) && $_SESSION['admin'] == true) {
  // el usuario es admin, muestra el boton para admin
  echo '<style>.admin__button { display: block; text-decoration: none; }</style>';
  // el usuario no es admin, no muestra el boton para admin
} else if ($_SESSION['admin'] == false) {
  echo '<style>.admin__button { display: none; }</style>';
  echo '<style>.profile { transform: translateX(1rem); }</style>';
}
?>

<body>
  <div class="nav" id="nav">
    <div>
      <h1 class="flix">FLIXHUB</h1>
    </div>
    <div class="profile">
      <div class="button_admin">
        <a href='cerrar-sesion-index.php'><button class=refs>Cerrar sesion</button></a>
        <a class="admin" style="text-decoration: none;" href="../trabajo-final/perfil-admin/admin-settings.php">
          <button class="admin__button">Admin Panel</button></a>
      </div>

      <?php

      // Conexión a la base de datos
      /*con alex
      $ip = "192.168.56.101:3306";
      $database = "DWES-php";
      $user = "admin";
      $pass = "admin";*/

      /*conexion sami*/
      $ip = "localhost";
      $database = "DWES-php";
      $user = "root";
      $pass = "Samanta3007";

      $conn = new mysqli($ip, $user, $pass, $database);

      // Verificar la conexión
      if ($conn->connect_error) {
        die("Error de conexión: " . $conn->connect_error);
      }

      if (isset($_SESSION['id'])) {
        $id = $_SESSION['id'];

        // Consulta para obtener la foto de perfil asociada al email
        $sql = "SELECT fotos_perfil.nombre_archivo 
              FROM usuarios 
              INNER JOIN fotos_perfil ON usuarios.foto_perfil_id = fotos_perfil.id 
              WHERE usuarios.id = '$id'";

        // Ejecutar la consulta
        $resultado = $conn->query($sql);

        // Verificar si se encontró la foto de perfil
        if ($resultado->num_rows > 0) {
          // Obtener el primer resultado
          $fila = $resultado->fetch_assoc();
          $foto_perfil = $fila['nombre_archivo'];
          // Mostrar la foto de perfil en la página
          echo '<a class="foto_perfil" href="../trabajo-final/perfil-usuario/actualizar_perfil.php"><img src="' . $foto_perfil . '" alt="Foto de perfil"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M169.4 470.6c12.5 12.5 32.8 12.5 45.3 0l160-160c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L224 370.8 224 64c0-17.7-14.3-32-32-32s-32 14.3-32 32l0 306.7L54.6 265.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l160 160z"/></svg></a>';
        } else {
          echo "No se encontró ninguna foto de perfil asociada al email '$id'";
        }
      } else {
        echo "No se ha enviado un formulario POST.";
      }
      ?>


    </div>
  </div>

  <!-- header-->
  <div class="banner">
    <div class="banner__contents">
      <h1 class="banner__title">Money Heist</h1>
      <div class="banner__buttons">
        <button class="banner__button">Play</button>
        <button class="banner__button">My List</button>
      </div>
      <div class="banner__description">
        To carry out the biggest heist in history, a mysterious man called The
        Professor recruits a band of eight robbers who a have a single
        caracteristic:
      </div>

    </div>
    <div class="banner--fedeBottom"></div>
  </div>
  <div class="content_row">
    <div class="row">
      <h2>FLIXHUB ORIGINAL</h2>
      <div class="row__posters">
        <?php

        // Consulta para obtener las películas principales
        $sql = "SELECT * FROM movies";
        $result = $conn->query($sql);

        // Mostrar las películas de la base de datos 
        if ($result->num_rows > 0) {
          while ($row = $result->fetch_assoc()) {

            // Mostrar cada película con su imagen
            echo "<img src='" . $row["foto_peli"] . "' alt='"  . "' class='row__poster row__posterLarge' />";

            echo "<div class='row__posters'>";

            echo "</div>";
          }
        } else {
          echo "No se encontraron películas.";
        }

        ?>
      </div>
    </div>

    <!--Trending Now -->
    <div class="row">
      <h2>Trending Now </h2>
      <div class="row__posters">
        <?php

        // Consulta para obtener las películas 
        $sql_treding = "SELECT * FROM movies_treding";
        $result_treding = $conn->query($sql_treding);

        // Mostrar las películas de la base de datos
        if ($result_treding->num_rows > 0) {
          while ($row = $result_treding->fetch_assoc()) {
            // Imprimir el título de la película y la imagen

            echo "<img src='" . $row["foto_peli"] . "' alt='" . $row["title"] . "' class='row__poster' />";
          }
        } else {
          echo "No se encontraron películas trending.";
        }
        ?>

      </div>
    </div>


    <!--Top Now -->
    <div class="row">
      <h2>Top Rated</h2>
      <div class="row__posters">
        <?php
        // Consulta para obtener las películas 
        $sql_rated = "SELECT * FROM movies_rated";
        $result_rated = $conn->query($sql_rated); //conectar bbdd

        // Mostrar las películas de bbdd
        if ($result_rated->num_rows > 0) {
          while ($row = $result_rated->fetch_assoc()) {

            echo "<img src='" . $row["foto_peli"] . "' alt='" . $row["title"] . "' class='row__poster' />";
          }
        } else {
          echo "No se encontraron películas de rated.";
        }
        ?>
      </div>
    </div>


    <!-- Romance Movies -->
    <div class="row">
      <h2>Romance Movies</h2>
      <div class="row__posters">
        <?php
        // Consulta para obtener las películas de romance
        $sql_romance = "SELECT * FROM movies_romance";
        $result_romance = $conn->query($sql_romance);

        // Mostrar las películas de la bbdd
        if ($result_romance->num_rows > 0) {
          while ($row = $result_romance->fetch_assoc()) {

            echo "<img src='" . $row["foto_peli"] . "' alt='" . $row["title"] . "' class='row__poster' />";
          }
        } else {
          echo "No se encontraron películas de romance.";
        }
        ?>
      </div>
    </div>
  </div>


  <script>
    //para el scroll
    const nav = document.getElementById("nav");

    window.addEventListener("scroll", () => {
      if (window.scrollY >= 100) {
        nav.classList.add("nav__black");
      } else {
        nav.classList.remove("nav__black");
      }
    });
  </script>
</body>

</html>