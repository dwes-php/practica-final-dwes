<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="stylespeli.css" />
  <title>Pagina principal</title>
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Bungee+Spice&display=swap" rel="stylesheet" />
  <style>
    * {
      margin: 0;
      box-sizing: border-box;
    }

    body {
      font-family: Arial, Helvetica, sans-serif;
      background-image: url(assets/imagen-back.jpg);
      background-color: rgba(0, 0, 0, 0.5);
      /* (0.5) para ajustar la opacidad */
      background-blend-mode: overlay;
      display: flex;
      justify-content: center;
      height: 100vh;
    }

    .title_logo {
      font-family: "Bungee Spice", sans-serif;
      font-size: 45px;
      text-shadow: 1px 2px 1px #fff, 5px 2px 1px #999;
      gap: 20px;
      padding: 20px;
    }

    .nav-index {
      position: fixed;
      top: 0;
      width: 100%;
      display: flex;
      justify-content: center;
      gap: 61.5%;
      padding: 1%;
      transition-timing-function: ease-in;
      /*para que el nav se mueva con el scrool*/
      transition: all 0.5s;
      
    }

    .nav-index__button {
      width: 100px;
      height: 35px;
      color: white;
      font-weight: 600;
      background-color: rgb(232, 128, 0);
      border-radius: 0.5vw;
      cursor: pointer;
      font-size: 17px;
      transform: translateY(20px);
      outline: none;
      border: none;
      /*transparente*/
    }

    .nav-index__button a {
      text-decoration: none;
      color: white;
    }

    .nav-index__button:hover {
      color: #fff;
      background-color: rgba(200, 119, 13, 0.975);
      /*transparente*/
      transition: all 0.2s;
    }

    .nav-index__button:active {
      transform: translateY(21px);
    }

    .principal__buttons {
      display: flex;
      justify-content: center;
    }

    .principal__button {
      width: 200px;
      height: 65px;
      color: white;
      font-weight: 700;
      background-color: rgb(232, 128, 0);
      border-radius: 0.5vw;
      cursor: pointer;
      font-size: 20px;
      outline: none;
      border: none;
      transform: translateY(-50px);

      /*transparente*/
    }

    .principal__button:hover {
      color: #fff;
      background-color: rgba(200, 119, 13, 0.975);
      /*transparente*/
      transition: all 0.2s;
    }

    .principal__button:active {
      transform: translateY(-48px);
    }

    .principal__texto {
      padding-top: 140px;
      height: 190px;
      transform: translateY(80px);
    }

    .principal__p {
      display: flex;
      justify-content: center;
      align-items: center;
      font-weight: bold;
      font-size: 40px;
      padding-bottom: 0.3rem;
      color: #fff;
      margin: 100px;
    }
  </style>
</head>

<body>
  <div class="nav-index" id="nav">
    <h1 class="title_logo">FLIXHUB</h1>
    <div>
      <a href="/EjerciciosPHP1/trabajo-final/usuarios/login.php">
        <button class="nav-index__button">Sign in</button></a>
    </div>
  </div>


  <div class="principal__texto">
    <h1 class="principal__p">
      Unlimited movies, TV shows, and more Watch
      <br />
      anywhere.
    </h1>

    <div class="principal__buttons">
      <a href="/EjerciciosPHP1/trabajo-final/usuarios/registro.php"><button class="principal__button">Get Started ></button></a>
    </div>
  </div>
</body>

</html>