<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bungee+Spice&display=swap" rel="stylesheet">
</head>
<style>
    body {
        margin: 0;
        padding: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 100vh;
        font-family: Arial, Helvetica, sans-serif;
        background-image: url(../assets/imagen-back.jpg);
        background-color: rgba(0, 0, 0, 0.5);
        /* (0.5) para ajustar la opacidad */
        background-blend-mode: overlay;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    header {
        position: fixed;
        top: 0;
        width: 115.5rem;
        padding: 5px;
    }

    .logo {
        width: 100px;
    }

    header h1 {
        font-family: "Bungee Spice", sans-serif;
        font-size: 45px;
        transform: translateX(10rem);
        text-shadow: 1px 2px 1px #fff, 5px 2px 1px #999;
        display: flex;
        align-items: center;
        gap: 20px;
    }

    .formulario {
        padding: 50px;
        width: 390px;
        height: 450px;
        background-color: white;
        background-color: rgb(0, 0, 0, 0.8);
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    }

    .title h1 {
        font-size: 35px;
        color: white;
    }

    .line {
        height: 2px;
        width: 490px;
        background-color: gray;
        transform: translateX(-85px);
    }

    .box-principal {
        width: 20rem;
    }

    .box {
        width: 300px;
        height: 25px;
        padding: 10px;
        font-weight: bold;
        font-size: 16px;
        color: white;
        background-color: rgb(0, 0, 0, 0.5);
        border: 1px solid grey;
        border-radius: 5px;
        transform: translateX(5px);
    }

    ::placeholder {
        color: white;
    }

    form {
        padding: 15px;
        translate: -21px 10px;
        width: 400px;
        height: 353px;
        display: flex;
        flex-direction: column;
        line-height: 4;
    }

    input[type="submit"] {
        width: 322px;
        height: 40px;
        color: white;
        font-weight: bold;
        background-color: rgb(232, 128, 0);
        border: 1px solid grey;
        border-radius: 5px;
        cursor: pointer;
        font-size: 20px;
        transform: translateY(20px) translateX(5px);
    }

    input[type="submit"]:active {
        transform: translateY(21px) translateX(5px);
    }

    .regis {
        height: 30px;
        color: white;
        transform: translateX(8px);
    }

    .msg {
        border: 1px solid red;
        position: absolute;
        padding: 20px;
        font-size: 20px;
        background-color: red;
        border-radius: 15px;
        transform: translateY(14rem);
        color: white;
    }
</style>

<?php
session_start();

if (isset($_SESSION["error_login"])) {
    $error_login = $_SESSION["error_login"];

    unset($_SESSION["error_login"]);
} else {
    
    $error_login = "";
}


    ?>


<body>
    <header>
        <h1>FLIXHUB</h1>
    </header>

    <section class="formulario">
        <div class="box-principal">
            <div class="title">
                <h1>Iniciar sesión</h1>
            </div>

            <div class="line"></div>

            <form method="POST">

                <div>
                    <input class="box" type="email" id="email" name="email" placeholder="Correo electrónico">
                </div>
                <div>
                    <input class="box" type="password" id="contraseña" name="contraseña" placeholder="Contraseña">
                </div>

                <div>
                    <input type="submit" value="Iniciar sesión" formaction="verificar-login.php">
                </div>

                <div class="regis">
                    <h5>¿Todavía no tienes una cuenta?</h5>
                </div>

                <div>
                    <input type="submit" value="Registrarte" formaction="registro.php">
                </div>
            </form>
        </div>


    </section>

    <?php
    if (!empty($error_login)) {
        echo '<p class="msg">' . $error_login . '</p>';
    }
    ?>
</body>

</html>