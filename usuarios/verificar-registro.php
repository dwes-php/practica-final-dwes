<?php

session_start();
/*con alex
$ip = "192.168.56.101:3306";
$database = "DWES-php";
$user = "admin";
$pass = "admin";*/

/*conexion sami*/

$ip = "localhost";
$database = "DWES-php";
$user = "root";
$pass = "Samanta3007";

$conexion = mysqli_connect($ip, $user, $pass) or die("No se ha podido conectar a la base de datos");
mysqli_select_db($conexion, $database) or die("No existe la base de datos");

// Verificar si se ha enviado un formulario de registro

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Verificar si todos los campos del formulario están llenos
    if (empty($_POST["nombre"]) || empty($_POST["apellidos"]) || empty($_POST["email"]) || empty($_POST["contraseña"])) {
        $_SESSION["error_registro"] = "Todos los campos son obligatorios";
        header("Location: registro.php");
        exit();
    }

    $nombre = $_POST["nombre"];
    $apellidos = $_POST["apellidos"];
    $email = $_POST["email"];
    $contraseña = $_POST["contraseña"];

    // Escapar los valores del formulario para prevenir la inyección de código
    $email = mysqli_real_escape_string($conexion, $_POST["email"]);     
    $contraseña = mysqli_real_escape_string($conexion, $_POST["contraseña"]);

    // Verificar si el email ya existe en la base de datos
    $query = "SELECT * FROM usuarios WHERE email = '$email'";
    $result = mysqli_query($conexion, $query);
    if (mysqli_num_rows($result) > 0) {
        // El email ya existe, redirigir al formulario de registro con un mensaje de error
        $_SESSION["error_registro"] = "!!El email ya está registrado";
        header("Location: registro.php");
        exit();
    } else {
        // El email no existe, registrar al nuevo usuario en la base de datos
        // Encriptar la contraseña antes de almacenarla en la base de datos 
        $contraseña_encriptada = password_hash($contraseña, PASSWORD_DEFAULT);
        $query = "INSERT INTO usuarios (nombre, apellido, email, pass, rol, foto_perfil_id) VALUES ('$nombre', '$apellidos', '$email', '$contraseña_encriptada', 'usuario', '2')";
        mysqli_query($conexion, $query);

        $id_usuario = mysqli_insert_id($conexion);

        

        // Redirigir a la página Mostrar.php
        header("Location: login.php");
        exit();
    }
} else {
    // Si se intenta acceder a VerificarRegistro.php directamente sin enviar el formulario, redirigir al formulario de registro
    header("Location: registro.php");
    exit();
}