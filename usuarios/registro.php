<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bungee+Spice&display=swap" rel="stylesheet">
</head>
<style>
    body {
        margin: 0;
        padding: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        font-family: Arial, Helvetica, sans-serif;
        background-image: url(../assets/imagen-back.jpg);
        background-color: rgba(0, 0, 0, 0.5);
        /* (0.5) para ajustar la opacidad */
        background-blend-mode: overlay;
    }

    header {
        position: fixed;
        top: 0;
        width: 115.5rem;
        padding: 5px;
    }

    header h1 {
        font-family: "Bungee Spice", sans-serif;
        font-size: 45px;
        transform: translateX(10rem);
        text-shadow: 1px 2px 1px #fff, 5px 2px 1px #999;
        display: flex;
        align-items: center;
        gap: 20px;
    }

    .logo {
        width: 100px;
    }

    .formulario {
        padding: 50px;
        width: 390px;
        height: 450px;
        background-color: white;
        background-color: rgb(0, 0, 0, 0.8);
    }

    .title {
        line-height: 0.2;
        color: white;
    }

    .title h1 {
        font-size: 35px;
    }

    .line {
        height: 3px;
        width: 490px;
        background-color: gray;
        transform: translateX(-50px);
    }

    .campos {
        display: flex;
        gap: 20px;
    }

    .box {
        width: 180px;
        height: 25px;
        padding: 8px;
        font-weight: bold;
        color: white;
        font-size: 15px;
        background-color: rgb(0, 0, 0, 0.5);
        border: 1px solid grey;
        border-radius: 5px;
    }

    .box2 {
        width: 395px;
        height: 25px;
        padding: 9px;
        font-weight: bold;
        color: white;
        font-size: 15px;
        background-color: rgb(0, 0, 0, 0.5);
        border: 1px solid grey;
        border-radius: 5px;
    }

    ::placeholder {
        color: white;
    }

    form {
        padding: 15px;
        translate: -21px 10px;
        width: 400px;
        height: 353px;
        display: flex;
        flex-direction: column;
        align-items: center;
        line-height: 4;
    }

    input[type="submit"] {
        width: 410px;
        padding: 15px;
        color: white;
        background-color: rgb(232, 128, 0);
        border: 1px solid grey;
        border-radius: 5px;
        cursor: pointer;
        font-size: 20px;
        transform: translateY(20px);
    }

    input[type="submit"]:active {
        transform: translateY(21px);
    }

    .msg {
        border: 1px solid red;
        position: absolute;
        padding: 20px;
        font-size: 20px;
        background-color: red;
        border-radius: 15px;
        transform: translateY(180px);
        color: white;
    }
</style>

<?php
session_start();

if (isset($_SESSION["error_registro"])) {
    $error_registro = $_SESSION["error_registro"];

    unset($_SESSION["error_registro"]);
} else {

    $error_registro = "";
}
?>

<body>
    <header>
        <h1>FLIXHUB</h1>
    </header>

    <section class="formulario">
        <div class="title">
            <h1>Registrarte</h1>
            <p>es rapido y fácil</p>
        </div>

        <div class="line"></div>

        <form action="verificar-registro.php" method="POST">
            <div class="campos">
                <input class="box" type="text" id="nombre" name="nombre" placeholder="Nombre">
                <input class="box" type="text" id="apellidos" name="apellidos" placeholder="Apellidos">
            </div>

            <div>
                <input class="box2" type="email" id="email" name="email" placeholder="Correo electrónico">
            </div>
            <div>
                <input class="box2" type="password" id="contraseña" name="contraseña" placeholder="Contraseña nueva">
            </div>
            <div>
                <input type="submit" value="Registrarte">
            </div>
        </form>

    </section>
    <?php
    if (!empty($error_registro)) {
        echo '<p class="msg">' . $error_registro . '</p>';
    }
    ?>

</body>

</html>