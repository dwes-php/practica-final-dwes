<?php

session_start();

/*con alex
$ip = "192.168.56.101:3306";
$database = "DWES-php";
$user = "admin";
$pass = "admin";*/

/*conexion sami*/
$ip = "localhost";
$database = "DWES-php";
$user = "root";
$pass = "Samanta3007";


$conexion = mysqli_connect($ip, $user, $pass) or die("No se ha podido conectar a la base de datos");
mysqli_select_db($conexion, $database) or die("No existe la base de datos");

// Verificar si se ha enviado un formulario de registro
if ($_SERVER["REQUEST_METHOD"] == "POST") {

  // Verificar si todos los campos del formulario están llenos
  if (empty($_POST["email"]) || empty($_POST["contraseña"])) {
    $_SESSION["error_login"] = "Todos los campos son obligatorios";
    header("Location: login.php");
    exit();
  }

  $email = $_POST["email"];
  $contraseña = $_POST["contraseña"];

  //preparacion para SQL inyection 
  $query = "SELECT * FROM usuarios WHERE email = ?"; //primer paso
    $stmt = mysqli_prepare($conexion, $query); //funcion para preparar la consulta para la ejecucion (2 argumentos)

    mysqli_stmt_bind_param($stmt, "s", $email); // el tipo de dato es un "s" string el que se va a asignar
    mysqli_stmt_execute($stmt);// ejecutar la consulta 

    // Obtener el resultado de la consulta
    $result = mysqli_stmt_get_result($stmt);

  if ($result) {
    if (mysqli_num_rows($result) > 0) {
      $usuario = mysqli_fetch_assoc($result);
      if (password_verify($contraseña, $usuario['pass'])) {
        //verifica el rol del usuario asociado al email
        $rol = $usuario['rol'];
        $id = $usuario['id'];
        $_SESSION['id'] = $id;

        if ($rol == 'admin') {
          $_SESSION['admin'] = true;
          // Las credenciales son válidas
          header("Location: ../indexpeli.php");
          exit();
        } else if ($rol == 'usuario') {
          $_SESSION['admin'] = false;
          // Las credenciales son válidas
          header("Location: ../indexpeli.php");
          exit();
        } else {
          // Rol desconocido
          $_SESSION["error_login"] = "Rol desconocido";
          header("Location: login.php");
          exit();
        }
      } else {
        // Contraseña incorrecta
        $_SESSION["error_login"] = "Contraseña incorrecta";
        header("Location: login.php");
        exit();
      }
    } else {
      // El email no existe en la base de datos
      $_SESSION["error_login"] = "El usuario no existe";
      header("Location: login.php");
      exit();
    }
  } else {
    // Error en la consulta
    $_SESSION["error_login"] = "Error en la consulta";
    header("Location: login.php");
    exit();
  }
}
