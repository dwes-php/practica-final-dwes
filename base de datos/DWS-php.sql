/*creacion de la base de datos*/
CREATE DATABASE `DWES-php`;
use `DWES-php`;

/*creacion de tablas*/
create TABLE usuarios (
id INT AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(255),
apellido VARCHAR(255),
email VARCHAR(255),
pass VARCHAR(255),
rol VARCHAR(255)
);

create TABLE movies (
id INT AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(255),
director VARCHAR(255),
año VARCHAR(255),
genero VARCHAR(255),
duracion VARCHAR(255),
sinopsis TEXT
);

create TABLE movies_treding (
id INT AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(255),
director VARCHAR(255),
año VARCHAR(255),
genero VARCHAR(255),
duracion INT,
foto_peli varchar(255)
);

create TABLE movies_rated(
id INT AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(255),
director VARCHAR(255),
año VARCHAR(255),
genero VARCHAR(255),
duracion INT,
foto_peli varchar(255)
);

create TABLE movies_romance(
id INT AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(255),
director VARCHAR(255),
año VARCHAR(255),
genero VARCHAR(255),
duracion INT,
foto_peli varchar(255)
);

CREATE TABLE fotos_perfil (
    id INT PRIMARY KEY,
    nombre_archivo VARCHAR(255)
);

/*relacion de la tabla usuarios con la tabla fotos_perfil*/
ALTER TABLE usuarios 
ADD COLUMN foto_perfil_id INT,
ADD CONSTRAINT fk_foto_perfil_id
    FOREIGN KEY (foto_perfil_id) REFERENCES fotos_perfil(id);

/*instertar datos en la tabla movies*/
ALTER TABLE movies add COLUMN foto_peli VARCHAR(255);
INSERT INTO movies (title, director, año, genero, duracion, sinopsis, foto_peli) 
VALUES 
('El Ultimátum de Bourne', 'Paul Greengrass', '2007', 'Acción, Misterio, Suspenso', '2h 15min', 'Un reportero británico pone a Jason Bourne sobre una nueva pista sobre su identidad, facilitándole el nombre de Blackbriar. Bourne dará con él en Londres, en un intento de encajar las últimas piezas de ese pasado que él aún intenta recuperar.', 'assets/large-movie1.jpg'),
('Venganza', 'Pierre Morel', '2008', 'Acción, Thriller', '1h 33min', 'El exagente de las fuerzas especiales de élite Bryan Millis se ve enredado en la trama de una organización criminal cuando trata de salvar a su hija, Kim.', 'assets/large-movie2.jpg'),
('Outside the Wire', 'Mikael Håfström', '2021', 'Acción, Aventura, Fantasía', '1h 54min', 'En un futuro cercano, un piloto de drones se encuentra emparejado con un oficial androide de alto secreto en una misión para detener un ataque nuclear.', 'assets/large-movie3.jpg'),
('Corazones de Acero', 'David Ayer', '2014', 'Acción, Drama, Bélico', '2h 14min', 'Al mando del veterano sargento Wardaddy, una brigada de cinco soldados americanos a bordo de un tanque, luchan contra un ejército nazi al borde de la desesperación ya que los alemanes sabían que su derrota estaba ya cantada por aquel entonces.', 'assets/large-movie4.jpg'),
('Terminator', 'James Cameron', '1984', 'Acción, Ciencia Ficción', '1h 47min', 'En el año 2029 las máquinas dominan el mundo. Los rebeldes que luchan contra ellas tienen como líder a John Connor, un hombre que nació en los años ochenta. Para eliminarlo y así acabar con la rebelión, las máquinas envían al pasado el robot Terminator con la misión de matar a Sarah Connor, la madre de John, e impedir así su nacimiento. Sin embargo, un hombre del futuro intentará protegerla.', 'assets/large-movie5.jpg'),
('12 Valientes', 'Nicolai Fuglsig', '2018', 'Acción, Drama, Historia', '2h 10min', 'La historia del primer equipo de Fuerzas Especiales desplegado en Afganistán tras el 11 de septiembre. Bajo el liderazgo de un nuevo capitán, el equipo debe derrotar a los talibanes y AL-Qaeda.', 'assets/large-movie6.jpg'),
('Fast & Furious 7', 'James Wan', '2015', 'Acción, Aventura, Crimen', '2h 17min', 'Hace un año que Dominic y Brian fueron indultados y pudieron regresar a los Estados Unidos. Después de su llegada desean adaptarse a su nueva vida dentro de la legalidad, pero las cosas no son tan fáciles. Dom quiere acercarse a Letty y Brian se acostumbra a la vida en una urbanización con Mia y su hijo.', 'assets/large-movie7.jpg'),
('Hunter Killer', 'Donovan Marsh', '2018', 'Acción, Thriller', '2h 2min', 'Un capitán de submarinos norteamericano hace equipo junto a los Navy Seals para intentar rescatar al presidente ruso, secuestrado por un general traidor.', 'assets/large-movie8.jpg'),
('El Protector', 'Prachya Pinkaew', '2005', 'Acción, Crimen, Thriller', '1h 51min', 'El expolicía de narcóticos Phil Broker, viudo y con una hija, decide mudarse a una localidad remota para escapar de su turbulento pasado. Sin embargo, Broker descubre que el pequeño pueblo esconde un trasfondo de violencia y drogas.', 'assets/large-movie9.jpg'),
('Top Gun', 'Tony Scott', '1986', 'Acción, Drama', '1h 50min', 'El joven piloto Maverick Mitchell es enviado a una prestigiosa escuela aérea, famosa por formar a los mejores pilotos de combate del país.', 'assets/large-movie10.jpg');

/*instertar datos en la tabla movies_romance*/
INSERT INTO movies_romance (title, director, año, genero, duracion, foto_peli) VALUES
('Me before You', 'Thea Sharrock', '2016', 'Romance', 106, 'assets/small_romance.jpg'),
('Diario de una pasión', 'Nick Cassavetes', '2004', 'Romance', 123, 'assets/small-romance1.jpg'),
('Shakespeare apasionado', 'John Madden', '1998', 'Romance', 123, 'assets/small-romance2.jpg'),
('Yo antes de ti', 'Thea Sharrock', '2016', 'Romance', 106, 'assets/small-romance3.jpg'),
('Love, Rosie', 'Christian Ditter', '2014', 'Romance', 102, 'assets/small-romance4.jpg'),
('Loco y estúpido amor', 'Glenn Ficarra, John Requa', '2011', 'Romance', 118, 'assets/small-romance5.jpg'),
('Un amor para recordar', 'Adam Shankman', '2002', 'Romance', 101, 'assets/small-romance6.jpg'),
('Realmente amor', 'Richard Curtis', '2003', 'Romance', 135, 'assets/small-romance7.jpg'),
('Mi primer amor', 'Howard Zieff', '1991', 'Romance', 99, 'assets/small-romance8.jpg'),
('La propuesta', 'Anne Fletcher', '2009', 'Romance', 108, 'assets/small-romance9.jpg'),
('A él no le gustas tanto', 'Ken Kwapis', '2009', 'Romance', 129, 'assets/small-romance10.jpg');

/*instertar datos en la tabla movies_treding*/
INSERT INTO movies_treding (title, director, año, genero, duracion, foto_peli)
VALUES 
('The Killer', 'John Smith', '2023', 'Acción, Thriller', 120, 'assets/small-trending1.jpg'),
('Blue Beetle', 'Maria Rodriguez', '2024', 'Acción, Ciencia ficción', 110, 'assets/small-trending2.jpg'),
('Teenage Mutant Ninja Turtles', 'Emily Thompson', '2025', 'Acción, Aventura, Comedia', 115, 'assets/small-trending3.jpg'),
('They Cloned Tyrone', 'Alicia Johnson', '2024', 'Ciencia ficción, Misterio, Drama', 105, 'assets/small-trending4.jpg'),
('Mission: Impossible - Dead Reckoning Part One', 'Christopher McQuarrie', '2023', 'Acción, Aventura, Thriller', 135, 'assets/small-trending5.jpg'),
('Spider-Man: Across the Spider - Verse', 'Phil Lord, Christopher Miller', '2024', 'Acción, Aventura, Animación', 125, 'assets/small-trending6.jpg'),
('Fast X', 'Justin Lin', '2025', 'Acción, Crimen, Drama', 140, 'assets/small-trending7.jpg'),
('Hypnotic', 'Robert Rodriguez', '2023', 'Misterio, Thriller, Ciencia ficción', 115, 'assets/small-trending8.jpg'),
('Guardians of the Galaxy Vol. 3', 'James Gunn', '2025', 'Acción, Aventura, Ciencia ficción', 150, 'assets/small-trending9.jpg'),
('Polite Society', 'Emma Thompson', '2024', 'Drama, Comedia, Crimen', 130, 'assets/small-trending10.jpg');

/*instertar datos en la tabla movies_rated*/
INSERT INTO movies_rated (title, director, año, genero, duracion, foto_peli)
VALUES 
('Dune', 'Denis Villeneuve', '2023', 'Ciencia ficción, Aventura', 155, 'assets/small-rated1.jpg'),
('Sisu', 'Juan Carlos Fresnadillo', '2024', 'Animación, Aventura, Comedia', 110, 'assets/small-rated2.jpg'),
('Guy Ritchie’s The Covenant', 'Guy Ritchie', '2023', 'Acción, Thriller', 120, 'assets/small-rated3.jpg'),
('Mighty Morphin Power Rangers', 'Jonathan Entwistle', '2025', 'Acción, Aventura, Fantasía', 130, 'assets/small-rated4.jpg'),
('Ride On', 'John Doe', '2024', 'Drama, Deportes', 120, 'assets/small-rated5.jpg'),
('Dungeons & Dragons', 'Jonathan Goldstein, John Francis Daley', '2023', 'Aventura, Fantasía', 125, 'assets/small-rated6.jpg'),
('John Wick: Chapter 4', 'Chad Stahelski', '2025', 'Acción, Crimen, Thriller', 140, 'assets/small-rated7.jpg'),
('Furies', 'Alicia Johnson', '2024', 'Drama, Misterio, Thriller', 115, 'assets/small-rated8.jpg'),
('Shazam! Fury of the Gods', 'David F. Sandberg', '2023', 'Acción, Aventura, Fantasía', 120, 'assets/small-rated9.jpg'),
('Unicorn Wars', 'Jane Smith', '2025', 'Aventura, Comedia, Fantasía', 105, 'assets/small-rated10.jpg');

/*instertar datos en la tabla fotos_perfil*/
INSERT INTO fotos_perfil (id, nombre_archivo) VALUES ('1', 'img_perfiles/perfil-1.jpg'),
('2','img_perfiles/perfil-2.jpg'),
('3','img_perfiles/perfil-3.jpg'),
('4','img_perfiles/perfil-4.jpg'),
('5','img_perfiles/perfil-5.jpg'),
('6','img_perfiles/perfil-6.jpg');




