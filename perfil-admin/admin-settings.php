<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Bungee+Spice&display=swap" rel="stylesheet">
</head>

<body>
   <?php

   session_start();
   if (isset($_SESSION["aviso"])) {
      $aviso = $_SESSION["aviso"];

      unset($_SESSION["aviso"]);
   } else {

      $aviso = "";
   }

   /*con alex

   $ip = "192.168.56.101:3306";
   $database = "DWES-php";
   $user = "admin";
   $pass = "admin";*/
   /*conexion sami*/
   $ip = "localhost";
   $database = "DWES-php";
   $user = "root";
   $pass = "Samanta3007";

   $conexion = mysqli_connect($ip, $user, $pass) or die("no se ha podido conectar a la base de datos");

   mysqli_select_db($conexion, $database) or die("no existe la base de datos");


   $consulta = "SELECT * FROM usuarios";


   $resultado = mysqli_query($conexion, $consulta);
   echo "<div class='as'>";
   echo "<a href='../indexpeli.php'><h1 class='title'>Usurarios registrados</h1></a>";
   echo "</div>";
   echo "<div class='div'>";
   echo "<table class='table'>";

   // Imprimir el título de la fila antes de iniciar el bucle
   echo "<tr class='titles'>";
   echo "<td class='background'>ID</td>";
   echo "<td class='background'>Nombre</td>";
   echo "<td class='background'>Apellido</td>";
   echo "<td class='background'>Email</td>";
   echo "<td class='background'>Rol</td>";
   echo "<td class='background'>Eliminar</td>";
   echo "<td class='background'>Modificar</td>";
   echo "</tr>";

   // Iterar sobre los resultados y mostrar cada fila
   while ($salida = mysqli_fetch_array($resultado)) {
      echo "<tr >";
      echo "<td>" . $salida['id'] . "</td>";
      echo "<td>" . $salida['nombre'] . "</td>";
      echo "<td>" . $salida['apellido'] . "</td>";
      echo "<td>" . $salida['email'] . "</td>";
      echo "<td>" . $salida['rol'] . "</td>";
      echo "<td><form method='post' action='eliminar-usuario.php'><input type='hidden' name='id' value='" . $salida['id'] . "'><input type='submit' class='delete' value='Eliminar'></form></td>";
      echo "<td><form class='select' method='post' action='modificar-usuario.php'><input type='hidden' name='id' value='" . $salida['id'] . "'><select id='opcion' name='opcion'><option value='usuario'>usuario</option><option value='admin'>admin</option></select><input type='submit' class='edit' value='Modificar'></form></td>";
      echo "</tr>";
   }

   echo "</table>";
   echo "</div>";
   
   if (!empty($aviso)) {
      echo '<p class="msg">' . $aviso . '</p>';
   }
   ?>



   <style>
      /* Estilo para el contenedor principal */
      body {
         margin: 0;
         padding: 0;
         display: flex;
         flex-direction: column;
         justify-content: center;
         align-items: center;
         height: 60rem;
         font-size: 20px;
         font-family: Arial, Helvetica, sans-serif;
         background-color: black;

      }

      .div {
         
         width: 500px;
         height: 400px;
         overflow-x: hidden;
         overflow-y: auto;
      }

      .div::-webkit-scrollbar-thumb {
         background-color: white;
         /* Color del pulgar de la barra de desplazamiento */
      }

      /* Estilo para el fondo */
      .background {
         background-color: wheat;
         color: black;
      }

      /* Estilo para el título */
      .title {
         font-family: "Bungee Spice", sans-serif;
         text-shadow: 1px 2px 1px #fff, 5px 2px 1px #999;
         top: 0;
         width: 100%;
         display: flex;
         justify-content: center;
         font-size: 50px;
      }

      a {
         text-decoration: none;
      }

      table {
         width: 95%;
         border-collapse: collapse;
         background-color: black;
         transform: translateY(-6.5rem);
      }

      th,
      td {
         border: 1px solid white;
         padding: 8px;
         text-align: center;
         color: white;
      }

      /* Estilo para filas pares */
      tr:nth-child(even) {
         background-color: #333;
      }

      /* Estilo para filas impares */
      tr:nth-child(odd) {
         background-color: #444;
      }

      .div {
         display: flex;
         flex-direction: column;
         align-items: center;
         width: 95%;
         padding: 5%;
      }

      /* Estilo para los botones */
      .delete {
         background-color: red;
         padding: 10px 20px;
         color: white;
         border: none;
         border-radius: 5px;
         font-weight: bold;
         cursor: pointer;
      }


      .edit {
         background-color: rgb(190, 139, 1);
         padding: 10px 20px;
         color: white;
         border: none;
         border-radius: 5px;
         font-weight: bold;
         cursor: pointer;
      }


      .select {
         display: flex;
         justify-content: center;
         gap: 15px;
      }

      select {
         background-color: black;
         padding: 8px 16px;
         border: 1px solid #ccc;
         border-radius: 5px;
         color: white;
         font-weight: bold;
      }

      .titles {
         position: sticky;
         top: 0;
      }
   </style>
</body>

</html>